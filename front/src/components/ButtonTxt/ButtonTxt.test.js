import React from "react";
import ButtonTxt from "./ButtonTxt";
import {render, screen} from "@testing-library/react";
import userEvent from "@testing-library/user-event";

describe(`Testing ButtonTxt.js`, () => {
  const testText = 'some action';

  test(`Smoke test ButtonTxt.js`, () => {
    render(<ButtonTxt text={testText}/>)

    expect(screen.getByTestId('txt-button')).toBeInTheDocument();
    expect(screen.getByText(`${testText}`)).toBeInTheDocument();
    expect(screen.getByTestId('txt-button')).toHaveAttribute(`class`);
    expect(screen.getByTestId('txt-button')).toHaveAttribute(`type`);
  });


  test(`Testing handleClick of ButtonTxt will be called`, () => {
    const testHandleClick = jest.fn();

    const {debug} = render(<ButtonTxt text={testText} onClick={testHandleClick}/>);
    expect(testHandleClick).not.toHaveBeenCalled();

    userEvent.click(screen.getByTestId('txt-button'));
    expect(testHandleClick).toHaveBeenCalled();
  });

});
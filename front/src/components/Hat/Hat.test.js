import React from "react";
import Hat from "./Hat";
import {render, screen} from "@testing-library/react";

describe(`Testing Hat.js`, () => {

  test(`Smoke test Hat.js`, () => {
    render(<Hat/>)

    expect(screen.getByTestId('test-hat')).toBeInTheDocument();
    expect(screen.getByTestId('test-hat')).toHaveAttribute(`class`);
  });
});
import {connect} from "react-redux";
import PrimarySearchAppBar from "../Nav/Nav"
import {createMuiTheme} from '@material-ui/core/styles';
import {ThemeProvider} from '@material-ui/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#656981",
    }
  },
});

const Header = (props) => {

  return (
    <header>
      <ThemeProvider theme={theme}>
        <PrimarySearchAppBar/>
      </ThemeProvider>
    </header>
  )
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps)(Header);
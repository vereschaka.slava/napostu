import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(0),
      cursor: "pointer",
      position: "inherit",
    },
  },

  avatarContentBox: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    overflow: "scroll",
    padding: "0 8px"
  },

  avatarContentBoxLarge: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    overflow: "scroll",
  },

  avatarPhotoBox: {
    padding: "4px",
  },

  avatarPhotoBoxBorder: {
    padding: "4px",
    borderRadius: "50%",
    border: "2px solid #ffd54b",
  },

  nickTextLink: {
    textDecoration: "none",
    fontSize: "0.7em",
    color: "#000000",
    width: "inherit",
    overflow: "scroll",
  },

  small: {
    position: "inherit",
    width: "48px",
    height: "48px",
    boxShadow: "0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",

  },

  medium: {
    position: "inherit",
    width: "72px",
    height: "72px",
    boxShadow: "0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",
  },

  large: {
    position: "inherit",
    width: "120px",
    height: "120px",
    boxShadow: "0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",
  },
}));

export default useStyles;
import {SET_POST_ACTIVE} from './types';

export const setPostActive = (data) => {
  return ({type: SET_POST_ACTIVE, payload: data});
}
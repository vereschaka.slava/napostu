import {SET_POST_ACTIVE} from './types';

const initialStatePost = {
  postActiveId: 0,
}

const reducer = (state = initialStatePost, action) => {
  switch (action.type) {
    case SET_POST_ACTIVE:
      return {...state, postActiveId: action.payload}
    default:
      return state;
  }
}

export default reducer;
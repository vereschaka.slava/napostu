import {SET_POSTS_I_S, SET_POSTS, SET_IS_POST_FAVORITE, UPDATE_LIKE_COUNTER, SHOW_HIDE_COMMENTS} from './types';


export const setPostsIS = (data) => {
  return ({type: SET_POSTS_I_S, payload: data});
};


export const setIsPostFavorite = (postId, userAct) => {
  return ({type: SET_IS_POST_FAVORITE, payload: {"postId": postId, "userAct": userAct}});
}


export const updateLikeCounter = (postId, userAct) => {
  return ({type: UPDATE_LIKE_COUNTER, payload: {"postId": postId, "userAct": userAct} });
};


export const showHideComments = (data) => {
  return ({type: SHOW_HIDE_COMMENTS, payload: data});
}



// without InfiniteScroll ____________
export const setPosts = (data) => {
  data.sort((a, b) =>{
    return (new Date(b.date)) - (new Date(a.date))
  });

  return ({type: SET_POSTS, payload: data});
};
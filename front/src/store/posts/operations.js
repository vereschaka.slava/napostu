import {LOAD_POSTS_REQUEST, LOAD_POSTS_SUCCESS,
  UPDATE_LIKE_COUNTER_REQUEST, UPDATE_LIKE_COUNTER_SUCCESS, LOAD_POSTS_FAILURE} from './types';
import axios from "axios";
import {requestHeaders} from "../../helpers/requests_helper";
import {setPosts, setPostsIS} from "./actions";


export const getPostsInfiniteScroll = (postsLength, userActiveId, userActiveSubscriptions) => (dispatch) => {
  dispatch({type: LOAD_POSTS_REQUEST})

  axios({
    method: 'post',
    url: '/getnextposts',
    data: `currentPostLength=${postsLength}&userActiveId=${userActiveId}&subscriptions=${userActiveSubscriptions}`,
    headers: requestHeaders
  })
    .then(res => {
      dispatch({type: LOAD_POSTS_SUCCESS})
      return res.data
    })
    .then(res => {
      return setStateFieldsToPostDataIS(res);
    })
    .then(res => {
      dispatch(setPostsIS(res));
    })
    .catch(err => {
      console.log(err);
    });
};

const setStateFieldsToPostDataIS = (data) => {
  data.postsToShow.map(el => {
    let favoritePostsState = JSON.parse(localStorage.getItem('favorites')) || [];

    (favoritePostsState.find(obj => +obj.post === el._id)) ?
      el.isPostFavorite = true : el.isPostFavorite = false;

    el.isCommentsShown = false;
    return el;
  })
  return data
};


export const updateLikeCounterBD = (postId, userAct) => (dispatch) => {
  axios({
    method: 'POST',
    url: '/updatelike',
    data: `id=${postId}&userAct=${userAct}`,
    headers: requestHeaders
  }).then( res => {
    dispatch({type: UPDATE_LIKE_COUNTER_REQUEST})
    return res;
  }).then( res => {
    dispatch({type: UPDATE_LIKE_COUNTER_SUCCESS})
  })
    .catch(err => {
      console.log(err);
    });
};


// without InfiniteScroll ____________
const pathPosts = `/postfeed`;

export const getPosts = () => (dispatch, getState) => {
  dispatch({type: LOAD_POSTS_REQUEST})

  axios(`${pathPosts}`)
    .then(res => {
      const data = res.data;
      console.log(data);
      dispatch({type: LOAD_POSTS_SUCCESS})
      return data;
    })
    .then(res => {
      const newData = setStateFieldsToPostData(res);
      dispatch(setPosts(newData));
    })
    .catch(error => {
      dispatch({type: LOAD_POSTS_FAILURE, payload: error})
    })

};

const setStateFieldsToPostData = (data) => {
  return data.map(el => {
    let favoritePostsState = JSON.parse(localStorage.getItem('favorites')) || [];

    (favoritePostsState.find(obj => +obj.post === el._id)) ?
      el.isPostFavorite = true : el.isPostFavorite = false;

    el.isCommentsShown = false;
    return el;
  })
};
